# WinterC++ Project Generator

A [Yeoman](http://yeoman.io/) generator for Winter C++ Projects.

[![Build Status](https://img.shields.io/badge/build-passing-green)]()
[![Yeoman](https://img.shields.io/badge/Yeoman-v4.8.0-red)](https://yeoman.github.io/generator/)
[![License](https://img.shields.io/badge/license-GPL%20v3-blue)](LICENSE.md)

| Feature                  | Dependency         | Version |
|--------------------------|--------------------|---------|
| Source Control           | Git                | 2.13.3  |
| Build & Packaging        | CMake              | 3.10    |
| DataBase Connector       | MySQLConnector C++ | 8.0.16  |
| DataBase Connector       | Redis              | 5.0.8   |
| Protocol Buffer Compiler | Protobuf           | 3.8.x   |
| RPC Framework            | gRPC               | 1.22.x  |
| Unit Testing             | Catch2             | x.x.x   |
| Documentation            | Doxygen            | x.x.x  |

## Getting Started

`yo` is the Yeoman command line utility allowing the creation of projects utilizing scaffolding templates (referred to as generators). Yo and the generators used are installed using [npm](https://www.npmjs.com/).

### Yeoman

To install Yeoman you can using this `npm` command:

```bash
npm install --global yo
```

## Install

To install this generator: 

 * First of all, clone this repo
 * Then, go to the project folder (`generator-winterCPP`) and run this `npm` command to install the WinterC++ Generator:

```bash
npm install --global 
```

## Setup

### Project

To interactively setup a project, create a new folder and go to there. Then run this inside:

```bash
yo winterCPlusPlus
```

To automatically setup a project you can try:

```bash
yo winterCPlusPlus --auto --name "sample" --author "Linus Torvalds" --email "ltorvalds@linux.com"
```

### Controller 

To add a controller interactively:

```bash
yo winterCPlusPlus:controller myController 
```

To add a service automatically:

```bash
yo winterCPlusPlus:controller myController --auto --author "Linus Torvalds" --email "ltorvalds@linux.com"
```

### Service 

To add a service interactively:

```bash
yo winterCPlusPlus:service myService
```

To add a service automatically:

```bash
yo winterCPlusPlus:service myService --auto --author "Linus Torvalds" --email "ltorvalds@linux.com"
```

### Repository 

To add a repository interactively:

```bash
yo winterCPlusPlus:repository myRepository
```

To add a repository automatically:

```bash
yo winterCPlusPlus:repository myRepository --auto --author "Linus Torvalds" --email "ltorvalds@linux.com"
```
