/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#ifndef <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_SERVICE_H
#define <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_SERVICE_H

#include <wintercpp/winter.h>
#include "role_service.h"
#include "../model/proto/<%= projectName %>.pb.h"
#include "../model/proto/util.pb.h"
#include "../repository/<%= projectName %>_repository.h"

using namespace winter;
using namespace protoModel;
// The name of the database's table
class <%= projectNameFirstUpperCase %>Service {

private:

    <%= projectNameFirstUpperCase %>Repository <%= projectName %>Repository;
    // TODO: Change or delete this, you can add other service you need to retrieve a class list here
    RoleService roleService;

public:

    <%= projectNameFirstUpperCase %> save(const <%= projectNameFirstUpperCase %> &<%= projectName %>);

    <%= projectNameFirstUpperCase %>List save<%= projectNameFirstUpperCase %>s(const <%= projectNameFirstUpperCase %>List &<%= projectName %>List);

    <%= projectNameFirstUpperCase %> findById(const std::string &id);

    <%= projectNameFirstUpperCase %> update<%= projectNameFirstUpperCase %>(const <%= projectNameFirstUpperCase %> &<%= projectName %>);

    <%= projectNameFirstUpperCase %>List <%= projectName %>FindAll();

    winter::proto::Pageable getPageable<%= projectNameFirstUpperCase %>(const PageableRequest &pageable);

    void validate(const <%= projectNameFirstUpperCase %> &<%= projectName %>);

};


#endif //<%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_SERVICE_H

