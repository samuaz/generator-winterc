/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#include <wintercpp/winter.h>
#include "<%= projectName %>_service.h"

using namespace sql;
using namespace winter;

<%= projectNameFirstUpperCase %> <%= projectNameFirstUpperCase %>Service::save(const <%= projectNameFirstUpperCase %> &<%= projectName %>) {
    validate(<%= projectName %>);
    return <%= projectName %>Repository.save(<%= projectName %>);
}

<%= projectNameFirstUpperCase %>List <%= projectNameFirstUpperCase %>Service::save<%= projectNameFirstUpperCase %>s(const <%= projectNameFirstUpperCase %>List &<%= projectName %>List) {
    for(const auto& <%= projectName %> : <%= projectName %>List.<%= projectName %>s()) {
        validate(<%= projectName %>);
        <%= projectName %>Repository.save(<%= projectName %>);
    }
    return <%= projectName %>List;
}

<%= projectNameFirstUpperCase %> <%= projectNameFirstUpperCase %>Service::findById(const std::string &id) {
    <%= projectNameFirstUpperCase %> <%= projectName %> = <%= projectName %>Repository.findById(id);
    // TODO: Example to retry a list of roles in the <%= projectName %> class, change this you could use this example to your list 
    VECTOR_TO_PROTO(<%= projectName %>.mutable_roles(),roleService.findRoleBy<%= projectNameFirstUpperCase %>Id(<%= projectName %>.uuid()));
    return <%= projectName %>;
}

<%= projectNameFirstUpperCase %> <%= projectNameFirstUpperCase %>Service::update<%= projectNameFirstUpperCase %>(const <%= projectNameFirstUpperCase %> &<%= projectName %>) {
    return <%= projectName %>Repository.update(<%= projectName %>);
}

<%= projectNameFirstUpperCase %>List <%= projectNameFirstUpperCase %>Service::<%= projectName %>FindAll() {
    <%= projectNameFirstUpperCase %>List <%= projectName %>List;
    VECTOR_TO_PROTO(<%= projectName %>List.mutable_<%= projectName %>s(), <%= projectName %>Repository.findAll());
    return <%= projectName %>List;
}

winter::proto::Pageable <%= projectNameFirstUpperCase %>Service::getPageable<%= projectNameFirstUpperCase %>(const PageableRequest &pageable) {
    return <%= projectName %>Repository.getPageableEntity(pageable.pagesize(), pageable.pagenumber(), "uuid");
}

void <%= projectNameFirstUpperCase %>Service::validate(const <%= projectNameFirstUpperCase %> &<%= projectName %>) {

    // TODO: Change this with the attributes of your model, you can validate the fields here
    if (<%= projectName %>.name().empty()) {
        throw <%= projectNameFirstUpperCase %>Exception("name is not set");
    }

    if (<%= projectName %>.lastname().empty()) {
        throw <%= projectNameFirstUpperCase %>Exception("lastName is not set");
    }

    if (<%= projectName %>.<%= projectName %>name().empty()) {
        throw <%= projectNameFirstUpperCase %>Exception("<%= projectName %>name is not set");
    }

    if (<%= projectName %>.email().empty()) {
        throw <%= projectNameFirstUpperCase %>Exception("email is not set");
    }

    if (<%= projectName %>.password().empty()) {
        throw <%= projectNameFirstUpperCase %>Exception("password is not set");
    }
}
