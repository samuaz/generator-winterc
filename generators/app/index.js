const Generator = require('yeoman-generator');
const chalk = require('chalk');
const moment = require('moment');

module.exports = class extends Generator {
    // Constructor
    constructor (args, opts) {
        super(args, opts);

        this.config.getAll();
        this.config.defaults(
            {
                projectName: 'projectName',
                projectDescription: 'A pretty cool winterC++ project.',
                projectPath: 'winterCPlusPlus',
                projectPackage: 'winterCPlusPlus_Project',
                projectLicense: 'GPL-3.0',
                isoYear: new Date().toISOString(),
                authorName: 'DIEGO LEONEL CAÑETE',
                authorEmail: 'diegoleonel.canete@gmail.com',
                companyName: 'The Open Company',
                companyId: 'theOpenCompany',
                companyWebsite: 'www.theOpenCompany.com',
                buildSharedLib: true,
                buildStaticLib: false,
                buildUtility: true,
                buildTest: true,
                unitTestType: 'catch',
                buildCodeCoverage: false,
                buildDocumentation: false
            }
        );

        this.interactive = true;

        this.option(
            'auto',
            {
                desc: 'Automatic mode',
                type: Boolean,
                default: false
            }
        );

        if (this.options.auto) { this.interactive = false; }

        this.argument('name', { type: String, required: false });
        this.argument('author', { type: String, required: false });
        this.argument('email', { type: String, required: false });

        if (this.options.name) {
            this.config.set('projectName', this.options.name);
            this.config.set('projectPath', this.options.name);
            this.config.set('projectPackage', this.options.name);
            this.interactive = false;
        }

        if (this.options.author) {
            this.config.set('authorName', this.options.author);
            this.interactive = false;
        }

        if (this.options.email) {
            this.config.set('authorEmail', this.options.email);
            this.interactive = false;
        }
    }

     // Run loop
    initializing () {
        let currentDate = moment();
        this.year = currentDate.format('YYYY');
        this.date = currentDate.format('D MMM YYYY');
    }

    prompting () {
        this.log(chalk.bold.blueBright(`
      ________  __         __                   ______     _        _ 
     |  |  |  ||__|.-----.|  |_ .-----..----.  |      | __| |__  __| |__ 
     |  |  |  ||  ||     ||   _||  -__||   _|  |   ---||__   __||__   __|
     |________||__||__|__||____||_____||__|    |______|   |_|      |_|      

        `));

        if (!this.interactive) { return ; }
        return this.prompt([
            {
                type: 'input',
                name: 'projectName',
                message: 'Project name in LowerCase (Ex.:  awesome-winterCPP-project):',
                default: this.config.get('projectName')
            },
            {
                type: 'input',
                name: 'projectDescription',
                message: 'Project description:',
                default: this.config.get('projectDescription')
            },
            {
                type: 'input',
                name: 'projectPath',
                message: 'Project path:',
                default: this.config.get('projectPath')
            },
            {
                type: 'input',
                name: 'projectPackage',
                message: 'Project package name:',
                default: this.config.get('projectPackage')
            },
            {
                type: 'input',
                name: 'projectLicense',
                message: 'Project license:',
                default: this.config.get('projectLicense')
            },
            {
                type: 'input',
                name: 'authorName',
                message: 'Author name:',
                default: this.config.get('authorName')
            },
            {
                type: 'input',
                name: 'authorEmail',
                message: 'Author e-mail:',
                default: this.config.get('authorEmail')
            },
            {
                type: 'input',
                name: 'companyName',
                message: 'Company name:',
                default: this.config.get('companyName')
            },
            {
                type: 'input',
                name: 'companyId',
                message: 'Company ID:',
                default: this.config.get('companyId')
            },
            {
                type: 'input',
                name: 'companyWebsite',
                message: 'Company website:',
                default: this.config.get('companyWebsite')
            }
        ])
        .then( (answers) => {
            this.config.set('projectName', answers['projectName']);
            this.config.set('projectNameUpperCase', setUpperCase(this.config.get('projectName')));
            this.config.set('projectNameFirstUpperCase', setFirstLetterUpperCase(this.config.get('projectName')));
            this.config.set('projectDescription', answers['projectDescription']);
            this.config.set('projectPath', answers['projectPath']);
            this.config.set('projectPackage', answers['projectPackage']);
            this.config.set('projectLicense', answers['projectLicense']);
            this.config.set('authorName', answers['authorName']);
            this.config.set('authorEmail', answers['authorEmail']);
            this.config.set('companyName', answers['companyName']);
            this.config.set('companyId', answers['companyId']);
            this.config.set('companyWebsite', answers['companyWebsite']);
        });
    }

    configuring() {  }

    default() {  }

    writing() {
        this._setupGit();
        this._setupBuild();
        this._setupSrc();
    }

    conflicts() {  }

    install() {  }

    end() {
        this._finish();
        this.config.save();
    }

    // Private methods
    _setupGit() {
        this.fs.copyTpl(
            this.templatePath('.npmignore'),
            this.destinationPath('.npmignore'),
            this.templatePath('_gitmodules'),
            this.destinationPath('.gitmodules'),
            {
                year: this.year,
                date: this.date,
                projectName: this.config.get('projectName'),
                projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
                projectPath: this.config.get('projectPath'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                companyName: this.config.get('companyName')
            }
        );
        this.spawnCommand('git', ['init']);
    }

    _setupBuild() {
        // CMAKE LISTS
        this.fs.copyTpl(
            this.templatePath('CMakeLists.txt'),
            this.destinationPath('CMakeLists.txt'),
            {
                projectName: this.config.get('projectName'),
                projectNameUpperCase: this.config.get('projectNameUpperCase'),
                projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );

        // DOCKER COMPOSE
        this.fs.copyTpl(
            this.templatePath('docker-compose.yml'),
            this.destinationPath('docker-compose.yml')
            // Here we can put the database data passing by prompting initializer.
        );

        // DOCKER FILES
        this.fs.copyTpl(
            this.templatePath('dockerfile/local'),
            this.destinationPath('dockerfile/local'),
            { projectName: this.config.get('projectName') }
        );
        this.fs.copyTpl(
            this.templatePath('dockerfile/develop'),
            this.destinationPath('dockerfile/develop'),
            { projectName: this.config.get('projectName') }
        );
        this.fs.copyTpl(
            this.templatePath('dockerfile/test'),
            this.destinationPath('dockerfile/test'),
            { projectName: this.config.get('projectName') }
        );
        this.fs.copyTpl(
            this.templatePath('dockerfile/release'),
            this.destinationPath('dockerfile/release'),
            { projectName: this.config.get('projectName') }
        );

        // LICENSE
        this.fs.copyTpl(
            this.templatePath('LICENSE'),
            this.destinationPath('LICENSE'),
            {
                year: this.year,
                date: this.date,
                projectName: this.config.get('projectName'),
                projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                companyName: this.config.get('companyName')
            }
        );

        // README
        this.fs.copyTpl(
            this.templatePath('README.md'),
            this.destinationPath('README.md'),
            {
                projectName: this.config.get('projectName'),
                projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
                projectDescription: this.config.get('projectDescription')
            }
        );
    }

    _setupSrc() {
        // LIBS FILES
        this.fs.write(this.destinationPath('winter/.gitkeep'), '');

        // CONFIG FILES
        this.fs.copyTpl(
            this.templatePath('src/config/winter_properties.h'),
            this.destinationPath('src/config/winter_properties.h'),
            {
                projectNameUpperCase: this.config.get('projectNameUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );
        this.fs.copyTpl(
            this.templatePath('src/config/winter_env.h'),
            this.destinationPath('src/config/winter_env.h'),
            {
                projectNameUpperCase: this.config.get('projectNameUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );
        this.fs.copyTpl(
            this.templatePath('src/config/winter_env.h.in'),
            this.destinationPath('src/config/winter_env.h.in'),
            {
                projectNameUpperCase: this.config.get('projectNameUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );

        // MAIN
        this.fs.copyTpl(
            this.templatePath('src/main.cpp'),
            this.destinationPath('src/main.cpp'),
            {
                projectName: this.config.get('projectName'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );

        // Entity Object
        const entityMapperObject = {
            projectName: this.config.get('projectName'),
            projectNameUpperCase: this.config.get('projectNameUpperCase'),
            projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
            authorName: this.config.get('authorName'),
            authorEmail: this.config.get('authorEmail'),
            isoYear: this.config.get('isoYear')
        };

        // CONTROLLER FILES
        this.fs.copyTpl(
            this.templatePath('src/controller/user_controller.cpp'),
            this.destinationPath('src/controller/' + this.config.get('projectName') + '_controller.cpp'), entityMapperObject
        );
        this.fs.copyTpl(
            this.templatePath('src/controller/user_controller.h'),
            this.destinationPath('src/controller/' + this.config.get('projectName') + '_controller.h'), entityMapperObject
        );
        // SERVICES FILES
        this.fs.copyTpl(
            this.templatePath('src/service/user_service.cpp'),
            this.destinationPath('src/service/' + this.config.get('projectName') + '_service.cpp'), entityMapperObject
        );
        this.fs.copyTpl(
            this.templatePath('src/service/user_service.h'),
            this.destinationPath('src/service/' + this.config.get('projectName') + '_service.h'), entityMapperObject
        );
        // REPOSITORY FILES
        this.fs.copyTpl(
            this.templatePath('src/repository/user_repository.cpp'),
            this.destinationPath('src/repository/' + this.config.get('projectName') + '_repository.cpp'), entityMapperObject
        );
        this.fs.copyTpl(
            this.templatePath('src/repository/user_repository.h'),
            this.destinationPath('src/repository/' + this.config.get('projectName') + '_repository.h'), entityMapperObject
        );
        // EXCEPTION FILES
        this.fs.copyTpl(
            this.templatePath('src/exception/user/user_exception.cpp'),
            this.destinationPath('src/exception/' + this.config.get('projectName') + '/' + this.config.get('projectName') + '_exception.cpp'),
            entityMapperObject
        );
        this.fs.copyTpl(
            this.templatePath('src/exception/user/user_exception.h'),
            this.destinationPath('src/exception/' + this.config.get('projectName') + '/' + this.config.get('projectName') + '_exception.h'),
            entityMapperObject
        );

        // MODELS FILES
        this.fs.write(this.destinationPath('src/model/proto/.gitkeep'), '');

        // PROTO FILES
        this.fs.copyTpl(
            this.templatePath('src/proto/user.proto'),
            this.destinationPath('src/proto/' + this.config.get('projectName') + '.proto'),
            {
                projectName: this.config.get('projectName'),
                projectNameUpperCase: this.config.get('projectNameUpperCase'),
                projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );
        this.fs.copyTpl(
            this.templatePath('src/proto/user-service.proto'),
            this.destinationPath('src/proto/' + this.config.get('projectName') + '-service.proto'),
            {
                projectName: this.config.get('projectName'),
                projectNameUpperCase: this.config.get('projectNameUpperCase'),
                projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
                authorName: this.config.get('authorName'),
                authorEmail: this.config.get('authorEmail'),
                isoYear: this.config.get('isoYear')
            }
        );
        this.fs.copyTpl(
            this.templatePath('src/proto/util.proto'),
            this.destinationPath('src/proto/util.proto')
        );

    }

    _finish() {
        this.log(
            chalk.bold.redBright(`
    ************************************************************************************************* `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.yellowBright(`                                       CONGRATULATIONS!!! `) +
    chalk.bold.redBright(`
    *`) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   The project was created in >> `) + chalk.bold.blueBright(`${this.destinationPath()} `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   WHAT IS THE NEXT? `) +
    chalk.bold.redBright(`
    *`) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   1. Clean and modify all the TODOs in order to handler your project `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   2. Configure you application object dependency and the proto files `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   3. Create a folders on >> `) + chalk.bold.blueBright(`$ENV{HOME}/winter-release/winter `) + chalk.bold.green(`with the WinterC++ lib `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   4. Install CMake, so run $> `) + chalk.bold.whiteBright(`cmake . `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   5. If you created with example, configure the database in >> `) + chalk.bold.blueBright(`config/winter_properties.h `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   6. You can start the database in a docker containers, so run $> `) + chalk.bold.whiteBright(`docker-compose up -d `) +
    chalk.bold.redBright(`
    *`) + chalk.bold.green(`   7. Take a coffee and thing your business logic. `) +
    chalk.bold.redBright(`
    *`) +
    chalk.bold.redBright(`
    ************************************************************************************************* `)
        );
    }
};

const setUpperCase = (string) => { return string.toUpperCase(); };
const setFirstLetterUpperCase = (string) => { return string.charAt(0).toUpperCase() + string.slice(1); };
