#
# Created by AZCONA VARGAS, SAMUEL EDUARDO
#

cmake_minimum_required(VERSION 3.10)
project(<%= projectNameUpperCase %>)
set(CMAKE_CXX_STANDARD 17)

#WE NEED TO SET THE PROJETNAME WITH APPNAME
option(APP_NAME "APP_NAME" hellowintercpp)
set(APP_NAME "<%= projectName %>")

if (ALPINE_BUILD)
    include(/usr/local/include/wintercpp/CMakeLists.txt)
else()
    ## symbolic link to WINTER SDK SOURCE
    execute_process(COMMAND rm -rf ${PROJECT_SOURCE_DIR}/winter || (exit 0))
    execute_process(COMMAND ln -fs $ENV{HOME}/winter-release/winter ${PROJECT_SOURCE_DIR}/winter
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})
    include(${PROJECT_SOURCE_DIR}/winter/CMakeLists.txt)
endif()

# include the winter cmakelists
add_executable(${PROJECT_NAME} src/main.cpp ${PROTOBUF_MODELS_INCLUDES} src/exception/<%= projectName %>/<%= projectName %>_exception.h src/exception/<%= projectName %>/<%= projectName %>_exception.cpp src/controller/<%= projectName %>_controller.h src/controller/<%= projectName %>_controller.cpp src/service/<%= projectName %>_service.h src/service/<%= projectName %>_service.cpp src/repository/<%= projectName %>_repository.h src/repository/<%= projectName %>_repository.cpp src/config/winter_properties.h src/config/winter_env.h.in)
target_link_libraries(${PROJECT_NAME} ${WINTER_LIBS} ${EXTRA_LIBRARY})
