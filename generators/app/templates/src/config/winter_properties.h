/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#ifndef <%= projectNameUpperCase %>_LOCAL_WINTER_PROPERTIES_H
#define <%= projectNameUpperCase %>_LOCAL_WINTER_PROPERTIES_H
#include <wintercpp/winter.h>

WINTER_CONFIG(R"(
develop:
  security:
    token_exp: 86400
    refresh_token_exp: 604800
    secret_key: NCrPpskR2twDOqG7gK0T9vyaFWi3AYJj
  grpc:
    host: 0.0.0.0
    port: 8080
  redis:
    host: redis-13393.c17.us-east-1-4.ec2.cloud.redislabs.com
    port: 13393
    password: ***TODO***
    initialpoolsize: 10
    maxpoolsize: 50
    usepool: true
  mysql:
    host: 10.115.144.3
    port: 3305
    name: ***TODO***
    user: ***TODO***
    password: ***TODO***
    initialpoolsize: 10
    maxpoolsize: 50
    reconnect: true
    connectiontimeout: 3600
    usepool: true
local:
  security:
    token_exp: 86400
    refresh_token_exp: 604800
    secret_key: NCrPpskR2twDOqG7gK0T9vyaFWi3AYJj
  grpc:
    host: 0.0.0.0
    port: 9090
  redis:
    host: localhost
    port: 6377
    password: ***TODO***
    initialpoolsize: 10
    maxpoolsize: 50
    usepool: true
    pooltimeout: 60
  mysql:
    host: localhost
    port: 3305
    name: ***TODO***
    user: ***TODO***
    password: ***TODO***
    initialpoolsize: 10
    maxpoolsize: 50
    reconnect: true
    usepool: false
    connectiontimeout: 3600
    pooltimeout: 60
master:
  security:
    token_exp: 86400
    refresh_token_exp: 604800
    secret_key: NCrPpskR2twDOqG7gK0T9vyaFWi3AYJj
  grpc:
    host: 0.0.0.0
    port: 8080
  redis:
    host: redis-13393.c17.us-east-1-4.ec2.cloud.redislabs.com
    port: 13393
    password: ***TODO***
    initialpoolsize: 10
    maxpoolsize: 50
    usepool: true
    pooltimeout: 60
  mysql:
    host: 35.226.83.220
    port: 3306
    name: ***TODO***
    user: ***TODO***
    password: ***TODO***
    initialpoolsize: 10
    maxpoolsize: 50
    reconnect: true
    usepool: true
    connectiontimeout: 3600
    pooltimeout: 60
)"
)
#endif //<%= projectNameUpperCase %>_LOCAL_WINTER_PROPERTIES_H
