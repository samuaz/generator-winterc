/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#include "<%= projectName %>_repository.h"
#include "../model/proto/util.pb.h"

<%= projectNameFirstUpperCase %> <%= projectNameFirstUpperCase %>Repository::save(const <%= projectNameFirstUpperCase %> &<%= projectName %>) {
    // TODO: Here you need to map your entity to SAVE the field on the table, this is a example
    INSERT("INSERT INTO <%= projectNameFirstUpperCase %>(name, lastName, gender, birthday, <%= projectName %>Name, email, type, profileImageLink, password, enabled) "
           "VALUES(?,?,?,STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s'),?,?,?,?,?,?)")
           .SET_VALUE(<%= projectName %>.name())
           .SET_VALUE(<%= projectName %>.lastname())
           .SET_VALUE(static_cast<int>(<%= projectName %>.gender()))
           .SET_VALUE(static_cast<sql::SQLString>(<%= projectName %>.birthday()))
           .SET_VALUE(<%= projectName %>.<%= projectName %>name())
           .SET_VALUE(<%= projectName %>.email())
           .SET_VALUE(static_cast<int>(<%= projectName %>.type()))
           .SET_VALUE(<%= projectName %>.profileimageurl())
           .SET_VALUE(Security::Password::encode(<%= projectName %>.password()))
           .SET_VALUE(<%= projectName %>.enabled()).EXECUTE(this);
    return findBy<%= projectNameFirstUpperCase %>Name(<%= projectName %>.<%= projectName %>name());
}

<%= projectNameFirstUpperCase %> <%= projectNameFirstUpperCase %>Repository::update(const <%= projectNameFirstUpperCase %> &<%= projectName %>) {
    // TODO: Here you need to map your entity to UPDATE the field on the table, this is a example
    QUERY("UPDATE <%= projectNameFirstUpperCase %> SET name = ?, lastName = ?, gender = ?, birthday = STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s'), "
          "<%= projectName %>Name = ?, email = ?, type = ?, profileImageLink = ?, enabled = ? WHERE id = unhex(?)");
    SET_STRING(<%= projectName %>.name());
    SET_STRING(<%= projectName %>.lastname());
    SET_ENUM(<%= projectName %>.gender());
    SET_DATE(<%= projectName %>.birthday());
    SET_STRING(<%= projectName %>.<%= projectName %>name());
    SET_STRING(<%= projectName %>.email());
    SET_ENUM(<%= projectName %>.type());
    SET_STRING(<%= projectName %>.profileimageurl());
    SET_BOOL(<%= projectName %>.enabled());
    SET_STRING(<%= projectName %>.uuid());
    UPDATE(query, <%= projectNameFirstUpperCase %>Exception)
    std::cout << status.message << "\n";
    return findById(<%= projectName %>.uuid());
}

<%= projectNameFirstUpperCase %> <%= projectNameFirstUpperCase %>Repository::createEntity(MYSQL_RESULTSET_RES) {
    // TODO: Here you need to map your entity to know the field on the table, this is a example
    <%= projectNameFirstUpperCase %> <%= projectName %>;
    <%= projectName %>.set_uuid(GET_STRING("uuid"));
    <%= projectName %>.set_name(GET_STRING("name"));
    <%= projectName %>.set_lastname(GET_STRING("lastName"));
    <%= projectName %>.set_email(GET_STRING("email"));
    <%= projectName %>.set_<%= projectName %>name(GET_STRING("<%= projectName %>Name"));
    <%= projectName %>.set_birthday(GET_STRING("birthday"));
    <%= projectName %>.set_type(<%= projectNameFirstUpperCase %>Type(GET_ENUM("type")));
    <%= projectName %>.set_gender(Gender(GET_ENUM("gender")));
    <%= projectName %>.set_enabled(GET_BOOL("enabled"));
    <%= projectName %>.set_creationdate(GET_DATE("createdIn"));
    <%= projectName %>.set_modificationdate(GET_DATE("updateIn"));
    VECTOR_TO_PROTO(<%= projectName %>.mutable_roles(), roleRepository.findRoleBy<%= projectNameFirstUpperCase %>Id(<%= projectName %>.uuid()));
    return <%= projectName %>;
}
