/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#ifndef <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_EXCEPTION_H
#define <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_EXCEPTION_H

#include <stdexcept>
#include <wintercpp/winter.h>
using namespace winter;
class <%= projectNameFirstUpperCase %>Exception : public WinterException {
public:
    explicit <%= projectNameFirstUpperCase %>Exception(char const *message) noexcept;
};


#endif //<%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_EXCEPTION_H

