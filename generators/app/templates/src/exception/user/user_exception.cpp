/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#include "<%= projectName %>_exception.h"

using namespace std;

<%= projectNameFirstUpperCase %>Exception::<%= projectNameFirstUpperCase %>Exception(char const *const message) noexcept : WinterException(message) {}
