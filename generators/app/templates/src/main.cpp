/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#include <iostream>
#include <cstdlib>
#include <vector>
#include <winterc++/winter.h>
#include "controller/<%= projectName %>_controller.h"
#include "config/properties.h"

int main() {
    WINTER_READ_CONFIG(winter::CONFIG);
    WINTER_SET_ENV("local");
    WINTER_ADD_CONTROLLER(<%= projectName %>Controller);
    WINTER_INIT;
    return 0;
}
