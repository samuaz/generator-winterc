/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#ifndef <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_CONTROLLER_H
#define <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_CONTROLLER_H

#include <grpcpp/grpcpp.h>
#include <wintercpp/winter.h>
#include "../service/<%= projectName %>_service.h"
#include "../model/proto/<%= projectName %>-service.grpc.pb.h"
#include "../model/proto/util.pb.h"


using namespace winter;
using grpc::Status;
using namespace protoModel;


/***
 * @class <%= projectNameFirstUpperCase %>Controller
 */
class <%= projectNameFirstUpperCase %>Controller final : GRPC_CONTROLLER(protoService::<%= projectNameFirstUpperCase %>::Service) {


private:

    <%= projectNameFirstUpperCase %>Service <%= projectName %>Service;

public:
    /***
     * @name create<%= projectNameFirstUpperCase %>
     * @param context
     * @param request
     * @param response
     * @return <%= projectNameFirstUpperCase %>Response
     */
    grpc::Status create<%= projectNameFirstUpperCase %>(ServerContext *context, const <%= projectNameFirstUpperCase %> *request, <%= projectNameFirstUpperCase %> *response);

    /***
     * @name get<%= projectNameFirstUpperCase %>
     * @param context
     * @param request
     * @param response
     * @return <%= projectNameFirstUpperCase %>Response
     */
    grpc::Status get<%= projectNameFirstUpperCase %>(ServerContext *context, const <%= projectNameFirstUpperCase %>FindRequest *request, <%= projectNameFirstUpperCase %> *response);

    /***
     * @name update<%= projectNameFirstUpperCase %>
     * @param context
     * @param request
     * @param response
     * @return <%= projectNameFirstUpperCase %>Response
     */
    grpc::Status update<%= projectNameFirstUpperCase %>(ServerContext *context, const <%= projectNameFirstUpperCase %> *request, <%= projectNameFirstUpperCase %> *response) override;

    /***
     * @name get<%= projectNameFirstUpperCase %>ListPageable
     * @param context
     * @param request
     * @param response
     * @return <%= projectNameFirstUpperCase %>PageableResponse
     */
    grpc::Status get<%= projectNameFirstUpperCase %>ListPageable(ServerContext *context, const PageableRequest *request, winter::proto::Pageable *response);
};


#endif //<%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_CONTROLLER_H
