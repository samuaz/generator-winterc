/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#include <wintercpp/winter.h>
#include "<%= projectName %>_controller.h"
#include "../model/proto/util.pb.h"

using namespace protoModel;

Status <%= projectNameFirstUpperCase %>Controller::create<%= projectNameFirstUpperCase %>(ServerContext *context, const <%= projectNameFirstUpperCase %> *request, <%= projectNameFirstUpperCase %> *reply) {

    RESPONSE_ENTITY(
            <%= projectNameFirstUpperCase %> <%= projectName %>;
            // TODO: Here you need to map your entity to know the field on the table
    <%= projectName %>.set_name(request->name());
    <%= projectName %>.set_lastname(request->lastname());
    <%= projectName %>.set_email(request->email());
    <%= projectName %>.set_<%= projectName %>name(request-><%= projectName %>name());
    <%= projectName %>.set_password(request->password());
    <%= projectName %>.set_birthday(request->birthday());
    <%= projectName %>.set_gender(request->gender());
    <%= projectName %>.set_type(request->type());
    <%= projectName %>.set_enabled(true);
    reply->CopyFrom(<%= projectName %>Service.save(<%= projectName %>));
    );

}

Status <%= projectNameFirstUpperCase %>Controller::create<%= projectNameFirstUpperCase %>s(ServerContext *context, const <%= projectNameFirstUpperCase %>List *request, <%= projectNameFirstUpperCase %>List *reply) {

    RESPONSE_ENTITY(
            SECURE;
    reply->CopyFrom(<%= projectName %>Service.save<%= projectNameFirstUpperCase %>s(*request));
    );
}

Status <%= projectNameFirstUpperCase %>Controller::get<%= projectNameFirstUpperCase %>(ServerContext *context, const <%= projectNameFirstUpperCase %>FindRequest *request, <%= projectNameFirstUpperCase %> *reply) {

    RESPONSE_ENTITY(
            SECURE;
    reply->CopyFrom(<%= projectName %>Service.findById(request->uuid()));
    );
}

Status <%= projectNameFirstUpperCase %>Controller::update<%= projectNameFirstUpperCase %>(ServerContext *context, const <%= projectNameFirstUpperCase %> *request, <%= projectNameFirstUpperCase %> *reply) {

    RESPONSE_ENTITY(
            SECURE;
    reply->CopyFrom(<%= projectName %>Service.update<%= projectNameFirstUpperCase %>(*request)););
}

Status <%= projectNameFirstUpperCase %>Controller::get<%= projectNameFirstUpperCase %>ListPageable(ServerContext *context, const PageableRequest *request,
                                           winter::proto::Pageable *reply) {
    RESPONSE_ENTITY(
            SECURE;
    reply->CopyFrom(<%= projectName %>Service.getPageable<%= projectNameFirstUpperCase %>(*request));
    );
}
