const Generator = require('yeoman-generator');
const chalk = require('chalk');
const moment = require('moment');

module.exports = class extends Generator {
    // Constructor
    constructor (args, opts) {
        super(args, opts);

        this.config.getAll();
        this.config.defaults(
            {
                projectName: 'projectName',
                authorName: 'DIEGO LEONEL CAÑETE',
                authorEmail: 'diegoleonel.canete@gmail.com',
                isoYear: new Date().toISOString()
            }
        );

        this.interactive = true;

        this.option(
            'auto',
            {
                desc: 'Automatic mode',
                type: Boolean,
                default: false
            }
        );

        if (this.options.auto) { this.interactive = false; }

        this.argument('name', { type: String, required: false });
        this.argument('author', { type: String, required: false });
        this.argument('email', { type: String, required: false });

        if (this.options.name) {
            this.config.set('projectName', this.options.name);
            this.interactive = false;
        }

        if (this.options.author) {
            this.config.set('authorName', this.options.author);
            this.interactive = false;
        }

        if (this.options.email) {
            this.config.set('authorEmail', this.options.email);
            this.interactive = false;
        }

    }

     // Run loop
    initializing () {
        let currentDate = moment();
        this.year = currentDate.format('YYYY');
        this.date = currentDate.format('D MMM YYYY');
    }

    prompting () {
        this.log(chalk.bold.blueBright(`
      ________  __         __                   ______     _        _ 
     |  |  |  ||__|.-----.|  |_ .-----..----.  |      | __| |__  __| |__ 
     |  |  |  ||  ||     ||   _||  -__||   _|  |   ---||__   __||__   __|
     |________||__||__|__||____||_____||__|    |______|   |_|      |_|      

        `));

        if (!this.interactive) { return ; }
        return this.prompt([
            {
                type: 'input',
                name: 'projectName',
                message: 'Project name in LowerCase (Ex.:  awesome-winterCPP-project):',
                default: this.config.get('projectName')
            },
            {
                type: 'input',
                name: 'authorName',
                message: 'Author name:',
                default: this.config.get('authorName')
            },
            {
                type: 'input',
                name: 'authorEmail',
                message: 'Author e-mail:',
                default: this.config.get('authorEmail')
            }
        ])
        .then( (answers) => {
            this.config.set('projectName', answers['projectName']);
            this.config.set('projectNameUpperCase', setUpperCase(this.config.get('projectName')));
            this.config.set('projectNameFirstUpperCase', setFirstLetterUpperCase(this.config.get('projectName')));
            this.config.set('authorName', answers['authorName']);
            this.config.set('authorEmail', answers['authorEmail']);
        });
    }

    configuring() {  }

    default() {  }

    writing() { this._setupController(); }

    conflicts() {  }

    install() {  }

    end() {
        this._finish();
        this.config.save();
    }

    _setupController() {
        // Entity Object
        const entityMapperObject = {
            projectName: this.config.get('projectName'),
            projectNameUpperCase: this.config.get('projectNameUpperCase'),
            projectNameFirstUpperCase: this.config.get('projectNameFirstUpperCase'),
            authorName: this.config.get('authorName'),
            authorEmail: this.config.get('authorEmail'),
            isoYear: this.config.get('isoYear')
        };

        // CONTROLLER FILES
        this.fs.copyTpl(
            this.templatePath('src/controller/user_controller.cpp'),
            this.destinationPath('src/controller/' + this.config.get('projectName') + '_controller.cpp'), entityMapperObject
        );
        this.fs.copyTpl(
            this.templatePath('src/controller/user_controller.h'),
            this.destinationPath('src/controller/' + this.config.get('projectName') + '_controller.h'), entityMapperObject
        );
    }

    _finish() {
        this.log(
            chalk.bold.redBright(`
    ***************************************************************************** `) +
            chalk.bold.redBright(`
    *`) + chalk.bold.yellowBright(`                            CONGRATULATIONS!!! `) +
            chalk.bold.redBright(`
    *`) +
            chalk.bold.redBright(`
    *`) + chalk.bold.green(`     Thank you for use WinterC++ Generator the Controller was created! `) +
            chalk.bold.redBright(`
    *`) +
            chalk.bold.redBright(`
    ***************************************************************************** `)
        );
    }
};

const setUpperCase = (string) => { return string.toUpperCase(); };
const setFirstLetterUpperCase = (string) => { return string.charAt(0).toUpperCase() + string.slice(1); };
