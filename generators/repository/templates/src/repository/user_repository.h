/**
 * @author <%= authorName %> - <%= authorEmail %>
 * @YEAR <%= isoYear %>
 */

#ifndef <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_REPOSITORY_H
#define <%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_REPOSITORY_H

#include <wintercpp/winter.h>
#include "../exception/<%= projectName %>/<%= projectName %>_exception.h"
#include "../model/proto/<%= projectName %>.pb.h"
#include "../model/proto/util.pb.h"

using namespace winter;
using namespace protoModel;
MYSQL_TABLE_DESCRIPTOR_BINARY(<%= projectNameFirstUpperCase %>, id)
class <%= projectNameFirstUpperCase %>Repository : public MysqlRepository<<%= projectNameFirstUpperCase %>, std::string, <%= projectNameFirstUpperCase %>Descriptor, <%= projectNameFirstUpperCase %>Exception> {

private:

public:

    <%= projectNameFirstUpperCase %> save(const <%= projectNameFirstUpperCase %> &entity) override;

    <%= projectNameFirstUpperCase %> update(const <%= projectNameFirstUpperCase %> &entity);

protected:

    <%= projectNameFirstUpperCase %> createEntity(MYSQL_RESULTSET_RES) override;

};


#endif //<%= projectNameUpperCase %>_LOCAL_<%= projectNameUpperCase %>_REPOSITORY_H
